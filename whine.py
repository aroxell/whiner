#!/usr/bin/python3

import argparse
import logging
import os
import re
import tarfile
import tempfile
import requests
from collections import defaultdict
import json
import difflib

logging.basicConfig(level=logging.DEBUG)

def get_file_or_url(path):
    if re.search(r'https?://', path):
        request = requests.get(path, allow_redirects=True)
        request.raise_for_status()
        filename = re.sub(r"[:/]", "_", path)
        with open(filename, 'wb') as f:
            f.write(request.content)
        return filename
    elif os.path.exists(path):
        return path
    else:
        raise Exception(f"Path {path} not found")


def write_json_obj(buffer, d, file_name, function_name, line_number, warn_err, analyzer):
    if buffer:
        d[file_name].setdefault(function_name, [])
        d[file_name][function_name].append({
            'line_number': line_number,
            'analyzer-checkers': analyzer,
            'warning_error': warn_err,
            'buffer': buffer,
        })


def get_json_object(file, args):
    infunction = False
    buffer = []
    warn_err = ""
    d = defaultdict(dict)
    file_name = ""
    function_name = ""
    line_number = 0
    lines = list(file.readlines())
    last_line = len(lines) - 1
    for i, line in enumerate(lines):
        if ": In function '" in line or i == last_line:
            if infunction:
                write_json_obj(buffer, d, file_name, function_name, line_number, warn_err, analyzer)
            buffer = [line]
            infunction = True
            if not i == last_line:
                file_name = line.split(":")[0]
                function_name = line.split("'")[1]
        elif line.startswith('make') or line.startswith('rm -r') \
                or line.startswith('mkdir ') or line.startswith('tar ') \
                or line.startswith('xz -T0 '):
            if infunction:
                write_json_obj(buffer, d, file_name, function_name, line_number, warn_err, analyzer)
            infunction = False
        elif ": warning: " in line or ": error: " in line:
            if args.clang or args.sparse:
                if infunction:
                    write_json_obj(buffer, d, file_name, function_name, line_number, warn_err, analyzer)
                buffer = [line]
                infunction = True
                if not i == last_line:
                    file_name = line.split(":")[0]
                    if len(line.split("'")) >= 2:
                        function_name = line.split("'")[1]
                    else:
                        function_name = None
            line_number = line.split(":")[1]
            warn_err = re.sub(r".*(warning|error): ", "", line.strip())
            analyzer = re.sub(r".*( \[|\] \[)", "[", line.strip()) or None
        else:
            if args.remove_line_numbers:
                line_no_re = re.compile(r'^([ ]*\|[ ]*)?([ ]*\d{1,5})([ ]*\|)')
                line_without_line_nos = line_no_re.sub(lambda m: (m.group(1) or '') + ' ' * len(m.group(2)) + m.group(3), line)
                buffer.append(line_without_line_nos)
            else:
                buffer.append(line)
    return json.dumps(d, indent = 3)


def main(args):
    output_dir = os.path.dirname(__file__)

    baselogfile = args.baselogfile[0]
    basefile = open(baselogfile, 'r')

    base_json_object = get_json_object(basefile, args)
    outfile_base = open(os.path.join(output_dir, os.path.splitext(baselogfile)[0] + '.json'), "w")
    outfile_base.write(base_json_object)
    outfile_base.close()

    outfile_base = open(os.path.join(output_dir, os.path.splitext(baselogfile)[0] + '.json'), "r")
    base_lines = list(outfile_base.readlines())

    if args.newlogfile[0]:
        newlogfile = args.newlogfile[0]
        newfile = open(newlogfile, 'r')
        new_json_object = get_json_object(newfile, args)

        outfile_new = open(os.path.join(output_dir, os.path.splitext(newlogfile)[0] + '.json'), "w")
        outfile_new.write(new_json_object)
        outfile_new.close()

        outfile_new = open(os.path.join(output_dir, os.path.splitext(newlogfile)[0] + '.json'), "r")
        new_lines = list(outfile_new.readlines())
    else:
        print(''.join(base_lines))
        exit(0)

    difference = list(difflib.unified_diff(base_lines, new_lines, fromfile=os.path.join(os.path.splitext(baselogfile)[0] + '.json'), tofile=os.path.join(os.path.splitext(newlogfile)[0] + '.json')))
    difference = ''.join(difference)
    print(difference)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--clang', action='store_true', default=False,
                        help="parse clang build logs")
    parser.add_argument('--remove-line-numbers', action='store_true', default=False,
                        help="remove line numbers")
    parser.add_argument('--sparse', action='store_true', default=False,
                        help="parse sparse build logs")
    parser.add_argument('baselogfile', nargs='?',
                        action='append',
                        type=get_file_or_url,
                        help="url or path to the log file")
    parser.add_argument('newlogfile', nargs='?',
                        action='append',
                        type=get_file_or_url,
                        help="url or path to the log file", default=None)
    args = parser.parse_args()
    if args:
        main(args)
    else:
        exit(1)
